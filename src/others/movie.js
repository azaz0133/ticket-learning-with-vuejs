import a from '../assets/poster/aquaman.jpg'
import b from '../assets/poster/avenger2.jpg'
import c from '../assets/poster/justice.jpg'


const movies = {
    "movies" : [
        { "id":"avenger","where":b},
        { "id":"aquaman","where":a},
        { "id":"justice","where":c}
    ],
    "justice":[
        {"id":"A1","seated":"true","price":120},
        {"id":"A2","seated":"true","price":120},
        {"id":"A3","seated":"true","price":120},
        {"id":"A5","seated":"false","price":1020},
        {"id":"A4","seated":"false","price":180},
        {"id":"A6","seated":"true","price":220},
        {"id":"A7","seated":"true","price":120}
    ],
    "avenger":[
        {"id":"A1","seated":"true","price":120},
        {"id":"A2","seated":"true","price":120},
        {"id":"A3","seated":"true","price":120},
        {"id":"A5","seated":"false","price":120},
        {"id":"A4","seated":"false","price":1080},
        {"id":"A6","seated":"false","price":220},
        {"id":"A7","seated":"true","price":120}
    ],
    "aquaman":[
        {"id":"A1","seated":"true","price":1200},
        {"id":"A2","seated":"true","price":1280},
        {"id":"A3","seated":"true","price":120},
        {"id":"A5","seated":"true","price":120},
        {"id":"A4","seated":"false","price":180},
        {"id":"A6","seated":"true","price":220},
        {"id":"A7","seated":"true","price":120}
    ]
}

export {
    movies
}